"use-strict";

const async = require("async");
const models = require("../models");

const chooseMentor = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "participant_id",
            value: req.body.participant_id
          },
          {
            name: "mentor_id",
            value: req.body.mentor_id
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function checkStatusPending(data, callback) {
        models.sequelize
          .query(
            "select * from mentorings where participant_id = '" +
              req.body.participant_id +
              "' and status = 1"
          )
          .then((result, err) => {
            if (result[0].length < 1)
              return callback({
                error: true,
                message: "You was register, please wait accepting from mentor",
                data: data
              });

            callback(null, result[0][0]);
          });
      },
      function checkStatusPending(data, callback) {
        models.sequelize
          .query(
            "select * from mentorings where participant_id = '" +
              req.body.participant_id +
              "' and status = 2"
          )
          .then((result, err) => {
            if (result[0].length < 1)
              return callback({
                error: true,
                message: "You was register",
                data: data
              });

            callback(null, result[0][0]);
          });
      },
      function createData(data, callback) {
        models.mentoring
          .create({
            participant_id: req.body.participant_id,
            mentor_id: req.body.mentor_id
          })
          .then((result, err) => {
            if (err)
              return callback({
                error: true,
                message: err,
                data: {}
              });

            callback(null, {
              error: false,
              message: "Success Choose Mentor, Please Wait For Accepting",
              data: req.body
            });
          });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const updateProfile = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function updateData(data, callback) {
        let userId = req.params.user_id;
        models.sequelize
          .query(
            "select a.user_id, a.name, a.gender, a.address, b.email, b.phone from participants as a join users as b on a.user_id = b.id where a.user_id = '" +
              userId +
              "'"
          )
          .then(result => {
            return result[0][0] || {};
          })
          .then(dataUser => {
            models.user
              .update(
                {
                  email: req.body.email || dataUser.email,
                  phone: req.body.phone || dataUser.phone
                },
                {
                  where: {
                    id: userId
                  }
                }
              )
              .then(() => {
                let reqParticipant = {
                  name: req.body.name || dataUser.name,
                  gender: req.body.gender || dataUser.gender,
                  address: req.body.address || dataUser.address
                };
                models.participant
                  .update(reqParticipant, {
                    where: {
                      user_id: userId
                    }
                  })
                  .then(() => {
                    console.log("success update participant: " + userId);
                  });
                callback(null, data);
              })
              .catch(err => {
                let error = {};
                console.log(err);
                error.error = true;
                error.message =
                  err.name == "SequelizeUniqueConstraintError"
                    ? err.errors[0].message + ", already in use"
                    : err.errors[0].message;
                callback(error);
              });
          });
      },
      function final(data, callback) {
        callback(null, {
          error: false,
          message: "Success update profile",
          data: {}
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const showProfile = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        let userId = req.params.user_id;
        models.sequelize
          .query(
            "select a.user_id, a.name, a.gender, a.address, b.email, b.phone, b.status from participants as a join users as b on a.user_id = b.id where a.user_id = '" +
              userId +
              "'"
          )
          .then((result, err) => {
            if (result[0].length < 1)
              return callback({
                error: true,
                message: "Not Found",
                data: data
              });

            callback(null, result[0][0]);
          });
      },
      function final(data, callback) {
        callback(null, {
          error: false,
          message: "Found",
          data: data
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const showLearningMethod = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        let mentorId = req.params.mentor_id;
        models.sequelize
          .query(
            "select a.mentor_id, a.title, a.description, b.user_id, b.name, b.gender, b.address, c.email, c.phone from learning_methods as a join mentors as b on a.mentor_id = b.id join users as c on b.user_id = c.id"
          )
          .then((result, err) => {
            if (result[0].length < 1)
              return callback({
                error: true,
                message: "Not Found",
                data: {}
              });

            callback(null, result[0]);
          });
      },
      function final(data, callback) {
        callback(null, {
          error: false,
          message: "Found",
          data: data
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const showMentor = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        let mentorId = req.params.mentor_id;
        models.sequelize
          .query(
            "select a.id, a.name, a.gender, a.address, b.phone from mentors as a join users as b on a.user_id = b.id"
          )
          .then((result, err) => {
            if (result[0].length < 1)
              return callback({
                error: true,
                message: "Not Found",
                data: {}
              });

            callback(null, result[0]);
          });
      },
      function final(data, callback) {
        callback(null, {
          error: false,
          message: "Found",
          data: data
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = {
  chooseMentor,
  updateProfile,
  showProfile,
  showLearningMethod,
  showMentor
};
