"use-strict";

const generateAccessToken = require("../utils/jwt");

const generate = async (req, res) => {
  let token = await generateAccessToken.generateTokenSecret();
  console.log(token);
  return res.json({
    token: token
  });
};

module.exports = { generate };
