# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 139.180.139.69 (MySQL 5.7.29-0ubuntu0.18.04.1)
# Database: db_mentoring
# Generation Time: 2020-11-30 11:43:12 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table learning_methods
# ------------------------------------------------------------

DROP TABLE IF EXISTS `learning_methods`;

CREATE TABLE `learning_methods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mentor_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `learning_methods` WRITE;
/*!40000 ALTER TABLE `learning_methods` DISABLE KEYS */;

INSERT INTO `learning_methods` (`id`, `mentor_id`, `title`, `description`, `created_at`, `updated_at`)
VALUES
	(1,1,'Mentoring Web','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','2020-11-30 17:36:00',NULL);

/*!40000 ALTER TABLE `learning_methods` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mentorings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mentorings`;

CREATE TABLE `mentorings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mentor_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `status` tinyint(11) DEFAULT '1' COMMENT '1 = pending, 2 = accept, 3 = decline',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mentorings` WRITE;
/*!40000 ALTER TABLE `mentorings` DISABLE KEYS */;

INSERT INTO `mentorings` (`id`, `mentor_id`, `participant_id`, `status`, `created_at`, `updated_at`)
VALUES
	(1,1,1,2,'2020-11-30 17:36:35','2020-11-30 17:38:03');

/*!40000 ALTER TABLE `mentorings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mentors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mentors`;

CREATE TABLE `mentors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL COMMENT 'male, female',
  `address` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mentors` WRITE;
/*!40000 ALTER TABLE `mentors` DISABLE KEYS */;

INSERT INTO `mentors` (`id`, `user_id`, `name`, `gender`, `address`, `created_at`, `updated_at`)
VALUES
	(1,1,'alex mentor','male ','indonesia','2020-11-30 17:34:51','2020-11-30 17:50:44');

/*!40000 ALTER TABLE `mentors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table participants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `participants`;

CREATE TABLE `participants` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL COMMENT 'male, female',
  `address` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `participants` WRITE;
/*!40000 ALTER TABLE `participants` DISABLE KEYS */;

INSERT INTO `participants` (`id`, `user_id`, `name`, `gender`, `address`, `created_at`, `updated_at`)
VALUES
	(1,2,'alex participant','male','indonesia','2020-11-30 17:35:27','2020-11-30 17:49:03');

/*!40000 ALTER TABLE `participants` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'mentor','Mentor','2020-11-28 23:45:02','2020-11-28 23:51:28'),
	(2,'participant','Participant','2020-11-28 23:45:05','2020-11-28 23:59:59');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `status` tinyint(11) DEFAULT '1' COMMENT '1 = active, 2 = non active',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `role_id`, `email`, `phone`, `password`, `status`, `created_at`, `updated_at`)
VALUES
	(1,1,'alexmentor@gmail.com','085236694759','e10adc3949ba59abbe56e057f20f883e',1,'2020-11-30 17:34:51',NULL),
	(2,2,'alexparticipant@gmail.com','085816203961','e10adc3949ba59abbe56e057f20f883e',1,'2020-11-30 17:35:27',NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
