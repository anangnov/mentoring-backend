"use strict";

module.exports = function(sequelize, DataTypes) {
  const learningMethod = sequelize.define(
    "learningMethod",
    {
      mentor_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    },
    {
      tableName: "learning_methods",
      timestamps: false
    }
  );

  return learningMethod;
};
