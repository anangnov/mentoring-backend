"use strict";

module.exports = function(sequelize, DataTypes) {
  const mentor = sequelize.define(
    "mentor",
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: true
      },
      address: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    },
    {
      tableName: "mentors",
      timestamps: false
    }
  );

  return mentor;
};
