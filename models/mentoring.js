"use strict";

module.exports = function(sequelize, DataTypes) {
  const mentoring = sequelize.define(
    "mentoring",
    {
      mentor_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      participant_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      status: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    },
    {
      tableName: "mentorings",
      timestamps: false
    }
  );

  return mentoring;
};
