"use strict";

module.exports = function(sequelize, DataTypes) {
  const participant = sequelize.define(
    "participant",
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true
      },
      gender: {
        type: DataTypes.STRING,
        allowNull: true
      },
      address: {
        type: DataTypes.TEXT,
        allowNull: true
      }
    },
    {
      tableName: "participants",
      timestamps: false
    }
  );

  return participant;
};
