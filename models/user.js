"use strict";

module.exports = function(sequelize, DataTypes) {
  const user = sequelize.define(
    "user",
    {
      role_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      status: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    },
    {
      tableName: "users",
      timestamps: false
    }
  );

  return user;
};
