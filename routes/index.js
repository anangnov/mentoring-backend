const express = require("express");
const router = express.Router();
const jwt = require("../utils/jwt");
const authToken = jwt.authenticateToken;
const testController = require("../controller/testController");
const tokenController = require("../controller/tokenController");
const authController = require("../controller/authController");
const participantController = require("../controller/participantController");
const mentorController = require("../controller/mentorController");

module.exports = app => {
  router.get("/test", testController.list);
  router.get("/generate-private-key", tokenController.generate);

  // Auth
  router.post("/register", authController.register);
  router.post("/login", authController.login);

  // Participant
  router.post(
    "/participant/choose-mentor",
    authToken,
    participantController.chooseMentor
  );
  router.get(
    "/participant/profile/:user_id",
    authToken,
    participantController.showProfile
  );
  router.post(
    "/participant/profile/update/:user_id",
    authToken,
    participantController.updateProfile
  );
  router.get(
    "/participant/learning-method",
    authToken,
    participantController.showLearningMethod
  );
  router.get(
    "/participant/mentor",
    authToken,
    participantController.showMentor
  );

  // Mentor
  router.post(
    "/mentor/accepting",
    authToken,
    mentorController.acceptingParticipant
  );
  router.post(
    "/mentor/learning-method/create",
    authToken,
    mentorController.createLearningMethod
  );
  router.get(
    "/mentor/learning-method/:mentor_id",
    authToken,
    mentorController.showLearningMethod
  );
  router.get(
    "/mentor/profile/:user_id",
    authToken,
    mentorController.showProfile
  );
  router.get(
    "/mentor/mentoring/:participant_id",
    authToken,
    mentorController.showParticipantByMentorId
  );
  router.post(
    "/mentor/profile/update/:user_id",
    authToken,
    mentorController.updateProfile
  );

  app.use("/api", router);
};
